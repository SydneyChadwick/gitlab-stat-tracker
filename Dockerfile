FROM amazoncorretto:17 as builder
WORKDIR application
COPY ./target/Counter.jar application.jar
RUN java -Djarmode=layertools -jar application.jar extract

FROM amazoncorretto:17
LABEL maintainer="Sydney Chadwick <cidwick@gmail.com>"
HEALTHCHECK --start-period=120s CMD curl --fail http://localhost:8080/actuator/health || exit 1
EXPOSE 8080
WORKDIR application
RUN yum -y install shadow-utils && yum clean all && rm -rf /var/cache/yum && useradd -u 5050 appUser
COPY --from=builder application/dependencies/ ./
COPY --from=builder application/spring-boot-loader/ ./
COPY --from=builder application/snapshot-dependencies/ ./
COPY --from=builder application/application/ ./
USER appUser
ENTRYPOINT ["java","-server","-Djava.security.egd=file:/dev/./urandom","-XX:-UseContainerSupport","-XX:MaxRAMPercentage=100", "org.springframework.boot.loader.JarLauncher"]
