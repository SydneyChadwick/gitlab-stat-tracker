# So what's this thing?

It's a tiny tool that allows you to have a look at some stats about projects on your GitLab repository.   
Note that it's still very much a work in progress, so there might be improvements in future.

# What's it DO?

Using the GitLab API, it'll attempt to list all the projects according to configuration in application.yml

# Samples

The following .yml will list projects on gitlab.com that I own:
```yml
counter:
  file:
    path: C:\counts
    name: myGitlabProjects.csv
    fullPath: ${counter.file.path}/${counter.file.name}
  extensions: .java, .xml
  languages: 
    useTop: true
  git:
    includeUserNamespaces: true
    checkout: true
    username: myUsername
    password: myPassword
    host: gitlab.com
    token: GitlabPrivateToken
    platform: gitlab-v4
    gitlab-v4:
      owned: true
      membership: true
```

With checkout set to `true`, the result will be as follows:  

| Project ID | Name | Web URL | Created | Last Updated | Language Response | Language | Important Code | .java | .xml | All Extensions |
|------------|------|---------|---------|--------------|-------------------|----------|----------------|-------|------|-------|
| 19525055 | Sydney/GitLab Project Language Counter | https://gitlab.com/SydneyChadwick/gitlab-project-language-counter | 2020-06-22T13:50:10.168Z | 2020-06-23T08:33:49.271Z | {"Java":100.0} | Java | 700 | 611 | 89 | {".java":611,".json":224,".md":44,".xml":89} |

With checkout set to `false`: (Note that the code inside projects is not counted)

| Project ID | Name | Web URL | Created | Last Updated | Language Response | Language | 
|------------|------|---------|---------|--------------|-------------------|----------|
| 19525055 | Sydney/GitLab Project Language Counter | https://gitlab.com/SydneyChadwick/gitlab-project-language-counter | 2020-06-22T13:50:10.168Z | 2020-06-23T08:33:49.271Z | {"Java":100.0} | Java |


# The params

```yml
counter:
  file:
    path: D:\counting # The folder to place files in (including temp folder if checkout = true)
    name: counts.csv # Name of the output CSV. There might still be some weirdness if the file already exists, as we append instead of overwrite.
    fullPath: ${counter.file.path}/${counter.file.name} #Full CSV file path
  extensions: .cs, .java, .js, .vue # File extensions that will have their own counts if checkout = true
  languages: 
    favoured: Vue, Java, "C#" # When determining languages, these results are given favour if they exist in the Gitlab API response
    useTop: false # Don't favour any languages, just use the top result
  git:
    badIds: 1, 2, 3 # If checkout = true, some projects have trouble cloning. THis allows you to skip projects for cloning and line counting
    includeUserNamespaces: false # Whether to include projects in user namespaces
    checkout: false # Clone all the projects, and count the lines of code in each one, according to extensions above
    username: # Gitlab username, for use when cloning projects (JGit)
    password: # Gitlab password, for use when cloning projects (JGit)
    host: # Your Gitlab repo host
    token: # Gitlab token, for use with API
    platform: gitlab-v4 # Which API Bean to use. Currently only Gitlab API V4 supported
    gitlab-v4: # Variables as in https://docs.gitlab.com/ee/api/projects.html#list-all-projects
      archived: 
      visibility: 
      search: 
      search_namespaces: 
      owned: 
      membership: 
      starred: 
      with_issues_enabled: 
      with_merge_requests_enabled: 
      id_after: 
      id_before: 
      last_activity_after: 
      last_activity_before: 
      min_access_level: 
      repository_checksum_failed: 
      wiki_checksum_failed: 
      with_programming_language: 
```
