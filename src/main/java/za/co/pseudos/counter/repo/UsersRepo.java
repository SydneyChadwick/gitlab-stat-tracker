package za.co.pseudos.counter.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import za.co.pseudos.counter.entity.Users;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface UsersRepo extends JpaRepository<Users, Integer> {

}
