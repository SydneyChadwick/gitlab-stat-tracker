package za.co.pseudos.counter.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import za.co.pseudos.counter.entity.Projects;

@RepositoryRestResource(collectionResourceRel = "projects", path = "projects")
public interface ProjectsRepo extends JpaRepository<Projects, Integer> {

}
