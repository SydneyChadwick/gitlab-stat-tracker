package za.co.pseudos.counter.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import za.co.pseudos.counter.entity.Maintainability;

@RepositoryRestResource(collectionResourceRel = "maintainabilities", path = "maintainabilities")
public interface MaintainabilityRepo extends JpaRepository<Maintainability, Integer> {

}
