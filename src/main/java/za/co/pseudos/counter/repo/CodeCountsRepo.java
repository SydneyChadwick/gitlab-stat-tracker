package za.co.pseudos.counter.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import za.co.pseudos.counter.entity.CodeCounts;

@RepositoryRestResource(collectionResourceRel = "codeCounts", path = "codeCounts")
public interface CodeCountsRepo extends JpaRepository<CodeCounts, String> {

    public List<CodeCounts> findByProjectId(Integer integer);
    public Optional<CodeCounts> findByProjectIdAndExtension(Integer projectId, String key);
}
