package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Event {

    private Integer id;
    private Integer projectId;
    
    @JsonProperty("action_name")
    private String actionName;
    
    @JsonProperty("author_username")
    private String authorUsername;
    
    @JsonProperty("push_data")
    private EventPushData pushData;

    private EventAuthor author;
}
