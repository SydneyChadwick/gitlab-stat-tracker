package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class Contributor {

    private String name;
    private Integer commits;
}
