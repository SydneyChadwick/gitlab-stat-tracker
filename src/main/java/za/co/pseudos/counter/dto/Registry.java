package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class Registry {

    private String name;
    private String path;
    private String location;
}
