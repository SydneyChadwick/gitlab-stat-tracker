package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class EventAuthor {
    
    private Integer id;
    private String name;
    private String username;
}
