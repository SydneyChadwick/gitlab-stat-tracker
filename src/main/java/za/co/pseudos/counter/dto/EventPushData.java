package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class EventPushData {
    
    @JsonProperty("commit_count")
    private Integer commitCount;
    
    private String action;
    private String ref;
    
    @JsonProperty("ref_type")
    private String refType;
    
    @JsonProperty("commit_title")
    private String commitTitle;

}
