package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class Branch {

    private String name;
    private Boolean merged;
}
