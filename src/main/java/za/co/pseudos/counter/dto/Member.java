package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Member {
    
    private String name;
    private String username;
    private String state;
    
    @JsonProperty(value = "access_level")
    private Integer accessLevel;
}
