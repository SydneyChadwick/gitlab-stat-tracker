package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProjectStatistics {

    @JsonProperty("commit_count")
    private Integer commitCount;
    
    @JsonProperty("storage_size")
    private Long storageSize;
    
    @JsonProperty("repository_size")
    private Long repositorySize;
    
    @JsonProperty("wiki_size")
    private Long wikiSize;
    
    @JsonProperty("lfs_objects_size")
    private Long lfsObjectSize;
    
    @JsonProperty("job_artifacts_size")
    private Long jobArtifactsSize;
    
    @JsonProperty("snippets_size")
    private Long snippetsSize;
}
