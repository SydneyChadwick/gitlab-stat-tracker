package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class Search {
    private String basename;
    private String path;
    private String filename;
    private String ref;
}
