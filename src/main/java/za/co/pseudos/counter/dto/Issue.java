package za.co.pseudos.counter.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Issue {
    
    private Integer id;
    private Integer iid;
    
    @JsonProperty(value = "project_id")
    private Integer projectId;
    
    private String title;
    private String description;
    private String state;
    
    @JsonProperty(value = "created_at")
    private String createdAt;
    
    @JsonProperty(value = "updated_at")
    private String updatedAt;
    
    @JsonProperty(value = "closed_at")
    private String closedAt;
    
    @JsonProperty(value = "closed_by")
    private IssueAuthor closedBy;
    
    private List<String> labels;
    private IssueAuthor author;
    private String type;
    private Integer upvotes;
    private Integer downvotes;
    
    @JsonProperty(value = "user_notes_count")
    private Integer userNotesCount;
    
    @JsonProperty(value = "merge_requests_count")
    private Integer mergeRequestsCount;
    
    @JsonProperty(value = "due_date")
    private String dueDate;
    
    @JsonProperty(value = "discussion_locked")
    private Boolean discussionLocked;
    
    @JsonProperty(value = "issue_type")
    private String issueType;
    
    @JsonProperty(value = "web_url")
    private String webUrl;
}
