package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class ProjectNamespace {

    private Integer id;
    private String name;
    private String path;
    private String kind;
    
    @JsonProperty("full_path")
    private String fullPath;
    
    @JsonProperty("parent_id")
    private Integer parentId;
    
    @JsonProperty("avatar_url")
    private Object avatarUrl;
    
    @JsonProperty("web_url")
    private String webUrl;

}