package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class IssueAuthor {

    private Integer id;
    private String name;
    private String username;
    private String state;
    
    @JsonProperty("avatar_url")
    private String avatarUrl;
    
    @JsonProperty("web_url")
    private String webUrl;
}
