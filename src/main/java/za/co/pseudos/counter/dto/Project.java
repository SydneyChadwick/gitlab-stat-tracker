package za.co.pseudos.counter.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class Project {
    private Integer id;
    private String description;
    private String name;
    private String path;
    
    private ProjectNamespace namespace;
    
    @JsonProperty("name_with_namespace")
    private String nameWithNamespace;

    @JsonProperty("path_with_namespace")
    private String pathWithNamespace;
    
    @JsonProperty("created_at")
    private String createdAt;
    
    @JsonProperty("http_url_to_repo")
    private String httpUrlToRepo;
    
    @JsonProperty("web_url")
    private String webUrl;
    
    @JsonProperty("readme_url")
    private String readmeUrl;
    
    @JsonProperty("last_activity_at")
    private String lastActivityAt;
    
    private Boolean archived;
    
    @JsonProperty("empty_repo")
    private Boolean emptyRepo;
    
    private ProjectStatistics statistics;
}
