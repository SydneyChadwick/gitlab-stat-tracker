package za.co.pseudos.counter.dto;

import lombok.Data;

@Data
public class RepositoryTree {

    private String name;
}
