package za.co.pseudos.counter.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

@Data   
@Configuration
@ConfigurationProperties(prefix = "counter.guess")
public class GuessConfig {

    private Map<String, String[]> extensions;
    private String[] ignore;
}
