package za.co.pseudos.counter.runner;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.service.Runner;

@Slf4j
@Service
@Profile("!test")
@ConditionalOnExpression("'${counter.runType.type:cmdRunner}'.equals(\"fixedDelay\")")
public class ScheduleRunnerDb {

    private Runner runner;
    
    @Scheduled(fixedDelayString = "${counter.runType.value:3600000}")
    public void run() throws Exception {
        
        runner.run();
        
    }
    
    @PostConstruct
    public void postContruct() {
        log.info("Using FixedDelay scheduled implementation");
    }
    
    @Autowired
    public void setRunner(Runner runner) {
        this.runner = runner;
    }
}
