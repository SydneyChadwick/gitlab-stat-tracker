package za.co.pseudos.counter.runner;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.service.Runner;

@Slf4j
@Service
@Profile("!test")
@ConditionalOnExpression("'${counter.runType.type:cmdRunner}'.equals(\"cmdRunner\")")
public class CmdRunnerDb implements CommandLineRunner {

    
    private Runner runner;

    @Override
    public void run(String... args) throws Exception {
        runner.run();

        System.exit(0);
    }
    
    @PostConstruct
    public void postContruct() {
        log.info("Using CommandLineRunner implementation");
    }

    @Autowired
    public void setRunner(Runner runner) {
        this.runner = runner;
    }

    

}
