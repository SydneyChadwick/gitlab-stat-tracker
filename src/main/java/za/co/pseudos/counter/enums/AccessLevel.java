package za.co.pseudos.counter.enums;

public enum AccessLevel {
    NO_ACCESS(0),
    GUEST(10),
    REPORTER(20),
    DEVELOPER(30),
    MAINTAINER(40),
    OWNER(50);
    
    public final Integer level;
    
    private AccessLevel(Integer level) {
        this.level = level;
    }
    
    public static AccessLevel fromLevel(Integer level) {
        for (AccessLevel e : values()) {
            if (e.level == level) {
                return e;
            }
        }
        return null;
    }
}
