package za.co.pseudos.counter.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.core.JsonProcessingException;

import za.co.pseudos.counter.dto.Contributor;
import za.co.pseudos.counter.dto.Event;
import za.co.pseudos.counter.dto.Member;
import za.co.pseudos.counter.dto.Project;
import za.co.pseudos.counter.entity.Projects;
import za.co.pseudos.counter.entity.Users;

public interface Counter {

    boolean parseLangResp(Projects project, ResponseEntity<String> langResp);

    void populateExtCols(Projects project, Map<String, Integer> importantCounts);

    void lineCounter(Projects projectE, Project project, boolean projectAdded) throws IOException, GitAPIException;

    boolean guessLang(Projects project, boolean projectAdded, Map<String, Integer> importantCounts);
    
    void getEventStats(List<Event> projectEvents, Users projectE) throws JsonProcessingException;

    void getContributorStats(List<Contributor> projectEvents, Users projectE) throws JsonProcessingException;

    void getProjectMembers(List<Member> projectMembers, List<Member> inherited, Users projectE)
            throws JsonProcessingException;
}
