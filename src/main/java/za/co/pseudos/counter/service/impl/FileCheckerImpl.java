package za.co.pseudos.counter.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.dto.Registry;
import za.co.pseudos.counter.dto.Search;
import za.co.pseudos.counter.entity.Maintainability;
import za.co.pseudos.counter.repo.MaintainabilityRepo;
import za.co.pseudos.counter.service.FileChecker;
import za.co.pseudos.counter.service.GitApi;

@Slf4j
@Service
@ConditionalOnMissingBean(type = "FileChecker")
public class FileCheckerImpl implements FileChecker {

    private static final String FRAMEWORK_VERSION = "Framework Version";
    private static final String TRUE_IS_GOOD = "TrueIsGood";
    private GitApi gitlabApi;
    private MaintainabilityRepo manRepo;

    @Override
    public void checkFiles(RestTemplate template, HttpEntity<String> request, Integer projectId,
            String language) throws JsonProcessingException {

        if (StringUtils.equalsAnyIgnoreCase(language, "Empty")) {
            return;
        }
        
        String ref = gitlabApi.getBranch(template, request, projectId);
        
        Maintainability man = manRepo.findById(projectId).orElse(new Maintainability(projectId));
        Map<String, List<Map<String, Object>>> results = new HashMap<>();
        List<Map<String, Object>> checkProj = new ArrayList<>();
        results.put("Project", checkProj);
        
        checkCiFile(checkProj, template, request, projectId, ref);
        checkDockerfile(checkProj, template, request, projectId, ref);
        checkStackFile(checkProj, template, request, projectId, ref);
        
        if (!StringUtils.isBlank(language)) {
            switch (language) {
            case "Java":
                List<Map<String, Object>> checkJava = new ArrayList<>();
                results.put("Java", checkJava);
                checkJava(checkJava, checkProj, template, request, projectId, ref);
                break;
            case "C#":
            case "C-Sharp":
                List<Map<String, Object>> checkCS = new ArrayList<>();
                results.put("C#", checkCS);
                checkNet(checkCS, checkProj, template, request, projectId, ref);
                break;
            case "Vue":
            case "JavaScript":
            case "Javascript":
                List<Map<String, Object>> checkJS = new ArrayList<>();
                results.put("JavaScript", checkJS);
                checkNode(checkJS, checkProj, template, request, projectId, ref);
                break;
            default:
                break;
            }
        }
        
        ObjectMapper objMap = new ObjectMapper();
        
        man.setResults(objMap.writeValueAsString(results));
        manRepo.save(man);
    }
    
    @Override
    public void checkCiFile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref) {

        String file = gitlabApi.getFile(template, request, projectId, ".gitlab-ci.yml", ref);

        man.add(Map.of("Has CI", file != null, TRUE_IS_GOOD, true));
        if (file != null) {
            man.add(Map.of("CI Has Production Jobs", StringUtils.containsAny(file, "- production", "-production", "- Production", "-Production"), TRUE_IS_GOOD, true));
        }
    }

    @Override
    public void checkDockerfile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request,
            Integer projectId, String ref) {

        String file = gitlabApi.getFile(template, request, projectId, "Dockerfile", ref);
        file = file == null ? gitlabApi.getFile(template, request, projectId, "dockerfile", ref) : file;

        if (file == null) {
            Optional<Search> search = gitlabApi.search(template, request, projectId, "FROM", "blobs").stream()
                    .filter(i -> i.getRef().equalsIgnoreCase("master") || i.getRef().equalsIgnoreCase("main"))
                    .filter(i -> StringUtils.containsIgnoreCase(i.getPath(), "Dockerfile")).findFirst();
            file = search.isPresent() ? gitlabApi.getFile(template, request, projectId, search.get().getPath(), ref) : file;
        }
        
        man.add(Map.of("Builds Docker Image", file != null));
        
        if (file != null) {
            List<Registry> images = gitlabApi.getRegistryImages(template, request, projectId);
            man.add(Map.of("Uses Git Docker Registry", images != null && !images.isEmpty(), TRUE_IS_GOOD, true));
            man.add(Map.of("Has Docker Healthcheck", StringUtils.containsIgnoreCase(file, "HEALTHCHECK"), TRUE_IS_GOOD, true));
        }
    }

    @Override
    public void checkStackFile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request,
            Integer projectId, String ref) {

        String file = gitlabApi.getFile(template, request, projectId, "stack.yml", ref);
        file = file == null ? gitlabApi.getFile(template, request, projectId, "compose.yml", ref) : file;
        file = file == null ? gitlabApi.getFile(template, request, projectId, "docker-compose.yml", ref) : file;

        man.add(Map.of("Has compose file", file != null, TRUE_IS_GOOD, true));
        
        if (file != null) {
            man.add(Map.of("Uses DFP", StringUtils.containsIgnoreCase(file, "com.df"), TRUE_IS_GOOD, false));
            man.add(Map.of("Uses Traefik", StringUtils.containsIgnoreCase(file, "traefik.enable"), TRUE_IS_GOOD, true));
            man.add(Map.of("Memory limits configured", StringUtils.containsIgnoreCase(file, "memory") && StringUtils.countMatches(file, "image") == StringUtils.countMatches(file, "memory"), TRUE_IS_GOOD, true));
            man.add(Map.of("Has Traefik Healthcheck", StringUtils.containsIgnoreCase(file, "loadbalancer.healthcheck"), TRUE_IS_GOOD, true));
        }
    }

    @Override
    public void checkJava(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref) {

        List<String> tree = gitlabApi.getRepoTree(template, request, projectId);

        man.add(Map.of("Is Maven", tree.contains("pom.xml"), TRUE_IS_GOOD, true));
        
        if (tree.contains("pom.xml")) {
            String file = gitlabApi.getFile(template, request, projectId, "pom.xml", ref);
            
            boolean isBoot = StringUtils.containsIgnoreCase(file, "spring-boot-starter-parent");
            man.add(Map.of("Is Spring Boot", isBoot, TRUE_IS_GOOD, true));

            if (isBoot) {
                String bootSection = StringUtils.substringBetween(file, "<parent>", "</parent>");
                proj.add(Map.of(FRAMEWORK_VERSION, StringUtils.substringBetween(bootSection, "<version>", "</version>")));
            }
        }

        man.add(Map.of("Uses Ivy", tree.contains("ivy.xml"), TRUE_IS_GOOD, false));
        man.add(Map.of("Uses Gradle", tree.contains("gradlew")));
        man.add(Map.of("Uses Netbeans", tree.contains("nbproject"), TRUE_IS_GOOD, false));
        
        man.add(Map.of("Basic Java", !tree.contains("ivy.xml") && !tree.contains("gradlew") && !tree.contains("nbproject") && !tree.contains("pom.xml"), TRUE_IS_GOOD, false));
    }

    @Override
    public void checkNet(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref) {

        List<Search> search = gitlabApi.search(template, request, projectId, ".csproj", "blobs");
        Optional<Search> slnFileO = search.stream().filter(i -> i.getFilename().endsWith(".sln")).findFirst();

        man.add(Map.of("SLN File found", !slnFileO.isEmpty(), TRUE_IS_GOOD, true));
        
        if (!slnFileO.isEmpty()) {
            String slnFile = gitlabApi.getFile(template, request, projectId, slnFileO.get().getPath(), ref);
            String[] projFiles = StringUtils.substringsBetween(slnFile, "Project(", "EndProject");

            List<String> targets = new ArrayList<>();
            for (String projString : projFiles) {
                String[] split = StringUtils.split(projString, ",");
                
                if (!split[1].contains(".csproj")) {
                    continue;
                }

                String pre = StringUtils.substringBeforeLast(slnFileO.get().getPath(), "/");
                String projFile;
                
                if (pre.endsWith(".sln")) {
                    projFile = gitlabApi.getFile(template, request, projectId, split[1].strip().replace("\"", "").replace("\\", "/"), ref);
                } else {
                    projFile = gitlabApi.getFile(template, request, projectId, pre + "/" + split[1].strip().replace("\"", "").replace("\\", "/"), ref);
                }

                man.add(Map.of("Has missing .csproj File", projFile == null, TRUE_IS_GOOD, false));
                    
                if (projFile != null) {
                    if (StringUtils.contains(projFile, "<TargetFramework>")) {
                        targets.add(StringUtils.substringBetween(projFile, "<TargetFramework>", "</TargetFramework>"));
                    } else if (StringUtils.contains(projFile, "<TargetFrameworks>")) {
                        targets.add(StringUtils.substringBetween(projFile, "<TargetFrameworks>", "</TargetFrameworks>"));
                    } else {
                        targets.add(StringUtils.substringBetween(projFile, "<TargetFrameworkVersion>",
                                "</TargetFrameworkVersion>"));
                    }
                }
            }

            targets = targets.stream().filter(Objects::nonNull).collect(Collectors.toList());
            
            if (!targets.isEmpty()) {
                log.debug("Targets: {}", targets);
                proj.add(Map.of(FRAMEWORK_VERSION, StringUtils.join(targets, "; ")));
                man.add(Map.of("Is .Net Core", StringUtils.contains(targets.get(0), "netcore") , TRUE_IS_GOOD, true));
                man.add(Map.of("Target versions match", Arrays.asList(targets).stream().distinct().count() <= 1, TRUE_IS_GOOD, true));
            }
        }
    }

    @Override
    public void checkNode(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref) {

        List<String> tree = gitlabApi.getRepoTree(template, request, projectId);

        man.add(Map.of("Has Package.json", tree.contains("package.json"), TRUE_IS_GOOD, true));

        if (tree.contains("package.json")) {
            String pkg = gitlabApi.getFile(template, request, projectId, "package.json", ref);
            man.add(Map.of("Is Vue", pkg.contains("vue"), TRUE_IS_GOOD, true));
            if (pkg.contains("vue")) {
                proj.add(Map.of(FRAMEWORK_VERSION, StringUtils.substringBetween(pkg, "vue\": \"", "\",")));
            }
        }

        if (tree.contains("bower.json")) {
            String bower = gitlabApi.getFile(template, request, projectId, "bower.json", ref);
            man.add(Map.of("Is Angular", bower.contains("angular"), TRUE_IS_GOOD, false));
            if (bower.contains("angular")) {
                proj.add(Map.of(FRAMEWORK_VERSION, StringUtils.substringBetween(bower, "angular\": \"", "\",")));
            }
        }
    }

    @Autowired
    public void setGitlabApi(GitApi gitlabApi) {
        this.gitlabApi = gitlabApi;
    }

    @Autowired
    public void setManRepo(MaintainabilityRepo manRepo) {
        this.manRepo = manRepo;
    }
}
