package za.co.pseudos.counter.service.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.dto.Contributor;
import za.co.pseudos.counter.dto.Event;
import za.co.pseudos.counter.dto.Issue;
import za.co.pseudos.counter.dto.Member;
import za.co.pseudos.counter.dto.Project;
import za.co.pseudos.counter.entity.Projects;
import za.co.pseudos.counter.entity.Users;
import za.co.pseudos.counter.repo.CodeCountsRepo;
import za.co.pseudos.counter.repo.ProjectsRepo;
import za.co.pseudos.counter.repo.UsersRepo;
import za.co.pseudos.counter.service.Counter;
import za.co.pseudos.counter.service.FileChecker;
import za.co.pseudos.counter.service.GitApi;
import za.co.pseudos.counter.service.Runner;

@Slf4j
@Service
public class RunnerImpl implements Runner {

    @Value("${counter.file.path}")
    private String filePath;
    @Value("${counter.extensions: #{null}}")
    private String[] extensions;

    @Value("${counter.git.checkout: #{false}}")
    private Boolean checkout;
    @Value("${counter.git.badIds: #{null}}")
    private Integer[] badIds;
    @Value("${counter.git.username: #{null}}")
    private String username;
    @Value("${counter.git.password: #{null}}")
    private String password;
    @Value("${counter.git.host}")
    private String host;
    @Value("${counter.git.token}")
    private String token;
    @Value("${counter.git.includeUserNamespaces: #{null}}")
    private Boolean includeUserNamespaces;

    private GitApi queryBuilder;
    private Counter counter;
    private FileChecker fileCheck;

    private ProjectsRepo projectsRepo;
    private UsersRepo usersRepo;
    private CodeCountsRepo codeCounts;
    
    @Override
    public void run() throws JsonProcessingException, ParseException {
        RestTemplate template = new RestTemplate();

        HttpHeaders head = new HttpHeaders();
        head.add("PRIVATE-TOKEN", token);
        HttpEntity<String> request = new HttpEntity<>(head);

        List<Project> list = queryBuilder.getProjectList(template, request);

        log.info("Total of {} projects found", list.size());

        if (!includeUserNamespaces) {
            list = list.stream().filter(i -> !i.getNamespace().getKind().equals("user")).collect(Collectors.toList());
            log.debug("Of which {} are not in user repos", list.size());
        }

        int count = 0;

        
        for (Project project : list) {

            count++;
            log.debug("Project: {} - {}", project.getId(), project.getWebUrl());
            Projects projectE = projectsRepo.findById(project.getId()).orElse(new Projects(project.getId()));
            
            projectE.setName(project.getNameWithNamespace().replace(" / ", "/"));
            projectE.setWebUrl(project.getWebUrl());
            projectE.setCreated(project.getCreatedAt());
            projectE.setLastUpdated(project.getLastActivityAt());
            projectE.setArchived(project.getArchived());
            projectE.setRepoSize(project.getStatistics().getRepositorySize());
            projectE.setStorageSize(project.getStatistics().getStorageSize());
            projectE.setWikiSize(project.getStatistics().getWikiSize());
            projectE.setArtifactSize(project.getStatistics().getJobArtifactsSize());

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            Calendar updated = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
            updated.setTime(df.parse(project.getLastActivityAt()));
            
            // Only check if it's not been checked before, and has changed since last check
            if (projectE.getLastChecked() == null || updated.after(projectE.getLastChecked()) || (checkout && codeCounts.findByProjectId(project.getId()).isEmpty())) {
                log.info("Checking: {} - {}", project.getId(), project.getName());

                                ResponseEntity<String> langResp = template.exchange(
                        "https://" + host + "/api/v4/projects/" + project.getId() + "/languages", HttpMethod.GET, request,
                        String.class, "");

                boolean projectAdded = false;
                if (project.getEmptyRepo().booleanValue()) {
                    projectAdded = true;
                    projectE.setLanguage("Empty");
                } else {
                    projectAdded = counter.parseLangResp(projectE, langResp);
                }
                
                List<Issue> issues = queryBuilder.getProjectIssues(template, request, project.getId(), "opened");
                projectE.setOpenIssues(Long.valueOf(issues.size()));
                
                Users users = usersRepo.findById(project.getId()).orElse(new Users(project.getId()));
                
                List<Event> projectEvents = queryBuilder.getEventList(template, request, project.getId());
                counter.getEventStats(projectEvents, users);
                
                List<Contributor> contributor = queryBuilder.getContributors(template, request, project.getId());
                counter.getContributorStats(contributor, users);
                
                List<Member> projectMembers = queryBuilder.getMemberList(template, request, project.getId(), false);
                List<Member> inheritedMembers = queryBuilder.getMemberList(template, request, project.getId(), true);
                counter.getProjectMembers(projectMembers, inheritedMembers, users);
    
                
                try {
                    usersRepo.save(users);
                } catch (Exception e) {
                    log.error("Error saving contributors of {} - {}", project.getId(), project.getName(), e);
                }
                
                
                // Only clone if checkout true, the project can be cloned, and it isn't archived
                if (checkout && (projectE.getCloneable() == null || projectE.getCloneable().booleanValue()) && !projectE.getArchived()) {
                    log.info("Cloning: {} - {}", project.getId(), project.getName());
                    try {
                        counter.lineCounter(projectE, project, projectAdded);
                        projectE.setCloneable(true);
                    } catch (Exception e) {
                        log.error("Error fetching line counts for project {} - {}. Skipping it.", projectE.getProjectId(), projectE.getName(), e);
                        projectE.setCloneable(false);
                    }
                }
                
                try {
                    fileCheck.checkFiles(template, request, project.getId(), projectE.getLanguage());
                } catch (Exception e) {
                    log.error("Error checking maintainability of {} - {}", project.getId(), project.getName(), e);
                }
            }
            
            if (count % 10 == 0) {
                log.debug("Project {} of {} done", count, list.size());
            }

            projectE.setLastChecked(Calendar.getInstance(TimeZone.getTimeZone("UTC")));
            projectsRepo.save(projectE);
        }
    }
    
    @Autowired
    public void setQueryBuilder(GitApi queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    @Autowired
    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    @Autowired
    public void setProjects(ProjectsRepo projects) {
        this.projectsRepo = projects;
    }

    @Autowired
    public void setFileCheck(FileChecker fileCheck) {
        this.fileCheck = fileCheck;
    }

    @Autowired
    public void setUsers(UsersRepo users) {
        this.usersRepo = users;
    }
    
    @Autowired
    public void setCodeCounts(CodeCountsRepo codeCounts) {
        this.codeCounts = codeCounts;
    }
}
