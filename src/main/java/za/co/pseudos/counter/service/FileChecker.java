package za.co.pseudos.counter.service;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface FileChecker {

    void checkCiFile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref);

    void checkNode(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref);

    void checkNet(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref);

    void checkStackFile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref);

    void checkDockerfile(List<Map<String, Object>> man, RestTemplate template, HttpEntity<String> request, Integer projectId, String ref);
    
    void checkJava(List<Map<String, Object>> man, List<Map<String, Object>> proj, RestTemplate template,
            HttpEntity<String> request, Integer projectId, String ref);

    void checkFiles(RestTemplate template, HttpEntity<String> request, Integer projectId,
            String language) throws JsonProcessingException;
}
