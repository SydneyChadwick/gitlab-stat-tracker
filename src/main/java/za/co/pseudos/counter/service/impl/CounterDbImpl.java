package za.co.pseudos.counter.service.impl;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.config.GuessConfig;
import za.co.pseudos.counter.dto.Contributor;
import za.co.pseudos.counter.dto.Event;
import za.co.pseudos.counter.dto.Member;
import za.co.pseudos.counter.dto.Project;
import za.co.pseudos.counter.entity.CodeCounts;
import za.co.pseudos.counter.entity.Projects;
import za.co.pseudos.counter.entity.Users;
import za.co.pseudos.counter.enums.AccessLevel;
import za.co.pseudos.counter.repo.CodeCountsRepo;
import za.co.pseudos.counter.service.Counter;

@Slf4j
@Service
@ConditionalOnMissingBean(type = "Counter")
public class CounterDbImpl implements Counter {

    @Value("${counter.languages.favoured: #{null}}")
    private String[] languages;
    @Value("${counter.languages.useTop: #{true}}")
    private Boolean useTopLanguage;

    @Value("${counter.git.username: #{null}}")
    private String username;
    @Value("${counter.git.token: #{null}}")
    private String password;

    @Value("${counter.file.path}")
    private String filePath;
    @Value("${counter.extensions: #{null}}")
    private String[] extensions;

    private CodeCountsRepo codeCounts;
    private GuessConfig guesses;

    @Override
    public boolean guessLang(Projects project, boolean projectAdded, Map<String, Integer> importantCounts) {

        boolean added = projectAdded;

        log.debug("Incoming extensions: {}", importantCounts);

        // Find extension with most lines, excluding docs and shapefiles as far as we
        // can
        Optional<Entry<String, Integer>> result = importantCounts.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(new Comparator<Integer>() {
                    @Override
                    public int compare(Integer o1, Integer o2) {
                        return o2.compareTo(o1);
                    }
                })).filter(i -> !StringUtils.equalsAnyIgnoreCase(i.getKey(), guesses.getIgnore())).findFirst();

        log.debug("Resulting extension: {}", result);

        if (result.isEmpty()) {
            log.warn("Unable to guess language for {} - {}, no resulting highest use extension after ignores.",
                    project.getProjectId(), project.getName());
            project.setLanguage("Documentation");
            return true;
        }

        String ext = result.get().getKey();

        for (Entry<String, String[]> guess : guesses.getExtensions().entrySet()) {
            if (StringUtils.equalsAnyIgnoreCase(ext, guess.getValue())) {
                project.setLanguage(guess.getKey());
                added = true;
            }
        }

        if (added == false) {
            log.warn("Unable to guess language, highest use extension ({}) for {} - {} is not on guess list", ext,
                    project.getProjectId(), project.getName());
        }

        return added;
    }

    @Override
    public boolean parseLangResp(Projects project, ResponseEntity<String> langResp) {

        log.debug("Language response: {}", langResp.getBody());

        if (langResp.getStatusCode().is4xxClientError()) {
            return false;
        }

        project.setLanguageResponse(langResp.getBody());
        JSONObject langRespB = new JSONObject(langResp.getBody());

        if (langRespB.isEmpty()) {
            return false;
        }

        boolean usedTop = false;
        if (!useTopLanguage) { // Compare to list of favoured languages
            for (String lang : languages) {
                if (langRespB.has(lang)) {
                    project.setLanguage(lang);
                    usedTop = true;
                }
            }
        }

        // Use the top used language if not already set
        if (!usedTop) {
            double max = 0;
            String top = null;
            for (Entry<String, Object> val : langRespB.toMap().entrySet()) {
                if ((double) val.getValue() > max) {
                    top = val.getKey();
                    max = (double) val.getValue();
                }
            }
            project.setLanguage(top);
        }

        return true;
    }

    @Override
    public void populateExtCols(Projects projectE, Map<String, Integer> importantCounts) {

        for (Entry<String, Integer> ic : importantCounts.entrySet()) {

            CodeCounts cnt = codeCounts.findByProjectIdAndExtension(projectE.getProjectId(), ic.getKey())
                    .orElse(new CodeCounts(projectE.getProjectId(), ic.getKey()));
            cnt.setValue(ic.getValue());
            codeCounts.save(cnt);
        }
    }

    @Override
    public void lineCounter(Projects projectE, Project project, boolean projectAdded)
            throws IOException, GitAPIException {

        File temp = new File(filePath + "/proj");

        try {
            if (temp.exists())
                FileUtils.deleteDirectory(temp);
        } catch (Exception e) {
            log.debug("Delete failed");
        }

        Git git = Git.cloneRepository().setURI(project.getHttpUrlToRepo())
                .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password)).setDirectory(temp)
                .call();
        git.close();

        Collection<File> files = FileUtils.listFiles(temp, null, true);

        Map<String, Integer> projLineCounts = new HashMap<>();

        int totalLines = 0;
        Map<String, Integer> importantCounts = new HashMap<>();

        for (File f : files) {

            if (f.getAbsolutePath().contains("\\.git\\") || f.getAbsolutePath().contains("/.git/")) {
                continue;
            }

            int lines = 0;

            LineIterator lineIterator = FileUtils.lineIterator(f);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                lines = !line.isBlank() ? lines + 1 : lines;
            }
            lineIterator.close();

            String ext = FilenameUtils.getExtension(f.getName());
            if (ext.isBlank()) {
                ext = f.getName();
            } else {
                ext = "." + ext;
            }

            projLineCounts.put(ext, projLineCounts.getOrDefault(ext, 0) + lines);
            totalLines += lines;
        }

        // Try to guess language if we haven't got it from Gitlab Languages API
        if (!projectAdded) {
            projectAdded = guessLang(projectE, projectAdded, importantCounts);
        }

        if (totalLines <= 2) {
            projectE.setLanguage("Empty");
        } else if (!projectAdded) {
            projectE.setLanguage("Unknown");
        }

        try {
            if (temp.exists())
                FileUtils.deleteDirectory(temp);
        } catch (Exception e) {
            log.debug("Delete failed");
        }

        populateExtCols(projectE, projLineCounts);

        log.debug("Project line counts: {}", projLineCounts);

    }

    @Override
    public void getEventStats(List<Event> projectEvents, Users projectE) throws JsonProcessingException {

        Map<String, Double> eventCounts = new HashMap<>();
        Integer total = 0;

        for (Event event : projectEvents) {
            if (event.getActionName().equalsIgnoreCase("pushed to") && (event.getPushData().getCommitTitle() == null
                    || !event.getPushData().getCommitTitle().startsWith("Merge branch"))) {
                eventCounts.put(event.getAuthor().getName(),
                        eventCounts.getOrDefault(event.getAuthor().getName(), 0D) + 1);
                total++;
            }
        }

        Map<String, Double> percents = new HashMap<>();
        for (Entry<String, Double> entry : eventCounts.entrySet()) {
            Double percent = (double) (entry.getValue() / total);

            percents.put(entry.getKey(),
                    BigDecimal.valueOf(percent * 100).setScale(2, RoundingMode.HALF_UP).doubleValue());
        }

        // Sort by highest contribution
        Map<String, Double> result = percents.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(new Comparator<Double>() {
                    @Override
                    public int compare(Double o1, Double o2) {
                        return o2.compareTo(o1);
                    }
                })).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));

        ObjectMapper mapper = new ObjectMapper();
        projectE.setContributionsLastYear(mapper.writeValueAsString(result));
    }

    @Override
    public void getContributorStats(List<Contributor> projectEvents, Users projectE) throws JsonProcessingException {

        Map<String, Double> eventCounts = new HashMap<>();
        Integer total = 0;

        for (Contributor event : projectEvents) {
            eventCounts.put(event.getName(), eventCounts.getOrDefault(event.getName(), 0D) + event.getCommits());
            total += event.getCommits();
        }

        Map<String, Double> percents = new HashMap<>();
        for (Entry<String, Double> entry : eventCounts.entrySet()) {
            Double percent = (entry.getValue() / total);
            percents.put(entry.getKey(),
                    BigDecimal.valueOf(percent * 100).setScale(2, RoundingMode.HALF_UP).doubleValue());
        }

        // Sort by highest contribution
        Map<String, Double> result = percents.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(new Comparator<Double>() {
                    @Override
                    public int compare(Double o1, Double o2) {
                        return o2.compareTo(o1);
                    }
                })).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));

        ObjectMapper mapper = new ObjectMapper();
        projectE.setContributors(mapper.writeValueAsString(result));
    }

    @Override
    public void getProjectMembers(List<Member> projectMembers, List<Member> inherited, Users projectE) throws JsonProcessingException {
        
        Map<AccessLevel, List<String>> result = projectMembers.stream()
                .collect(Collectors.groupingBy(i -> AccessLevel.fromLevel(i.getAccessLevel()), Collectors.mapping(Member::getName, Collectors.toList()))).entrySet().stream()
                .sorted(Map.Entry.comparingByKey(new Comparator<AccessLevel>() {
                    @Override
                    public int compare(AccessLevel o1, AccessLevel o2) {
                        return o2.level.compareTo(o1.level);
                    }
                })).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
        
        Map<AccessLevel, List<String>> groupResult = inherited.stream()
                .collect(Collectors.groupingBy(i -> AccessLevel.fromLevel(i.getAccessLevel()), Collectors.mapping(Member::getName, Collectors.toList()))).entrySet().stream()
                .sorted(Map.Entry.comparingByKey(new Comparator<AccessLevel>() {
                    @Override
                    public int compare(AccessLevel o1, AccessLevel o2) {
                        return o2.level.compareTo(o1.level);
                    }
                })).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
        
        ObjectMapper mapper = new ObjectMapper();
        projectE.setMembers(mapper.writeValueAsString(result));
        projectE.setInheritedMembers(mapper.writeValueAsString(groupResult));
    }

    // Set some sensible default guesses
    @PostConstruct
    public void post() {

        if (guesses == null) {
            guesses = new GuessConfig();
        }

        if (guesses.getExtensions() == null) {
            Map<String, String[]> map = new HashMap<>();
            map.put("C-Sharp", new String[] { ".aspx", ".ashx", ".cs", ".sln" });
            map.put("Vue", new String[] { ".vue", ".vuex" });
            map.put("Java", new String[] { ".java", ".jsp", ".pom" });
            map.put("Javascript", new String[] { ".js" });
            map.put("Yaml", new String[] { ".yml" });
            map.put("Python", new String[] { ".py" });
            map.put("Php", new String[] { ".php" });
            map.put("Ruby", new String[] { ".rb" });
            map.put("Freemarker", new String[] { ".ftl" });
            map.put("Pentaho", new String[] { ".kdb", ".kjb" });
            guesses.setExtensions(map);
        }

        if (guesses.getIgnore() == null) {
            guesses.setIgnore(new String[] { ".md", ".gitignore", ".doc", ".docx", ".xlsx", ".xls", ".pdf", ".txt" });
        }

        log.info("Guessable extension map: {}", guesses);
    }

    @Autowired
    public void setCodeCounts(CodeCountsRepo codeCounts) {
        this.codeCounts = codeCounts;
    }

    @Autowired
    public void setGueses(GuessConfig guesses) {
        this.guesses = guesses;
    }
}
