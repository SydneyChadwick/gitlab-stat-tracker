package za.co.pseudos.counter.service.impl;

import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import za.co.pseudos.counter.dto.Branch;
import za.co.pseudos.counter.dto.Contributor;
import za.co.pseudos.counter.dto.Event;
import za.co.pseudos.counter.dto.Issue;
import za.co.pseudos.counter.dto.Member;
import za.co.pseudos.counter.dto.Project;
import za.co.pseudos.counter.dto.Registry;
import za.co.pseudos.counter.dto.RepositoryTree;
import za.co.pseudos.counter.dto.Search;
import za.co.pseudos.counter.service.GitApi;

@Data
@Slf4j
@Service
@ConfigurationProperties(prefix = "counter.git.gitlab-v4")
@ConditionalOnProperty(havingValue = "gitlab-v4", prefix = "counter.git", name = "platform", matchIfMissing = true)
public class GitlabApiV4 implements GitApi {

    private Boolean archived;
    private String visibility;
    private String search; 
    private Boolean searchNamespaces;
    private Boolean owned;
    private Boolean membership;
    private Boolean starred;
    private Boolean withIssuesEnabled;
    private Boolean withMergeRequestsEnabled;
    private String withProgrammingLanguage; 
    private Boolean wikiChecksumFailed; 
    private Boolean repositoryChecksumFailed; 
    private Integer minAccessLevel; 
    private Integer idAfter; 
    private Integer idBefore; 
    private String lastActivityAfter; 
    private String lastActivityBefore; 
    
    @Value("${counter.git.host}")
    private String host;
    
    @Override
    public List<Project> getProjectList(RestTemplate template, HttpEntity<String> request) {
        URI query = buildProjectQuery();

        ResponseEntity<Project[]> response = template.exchange(query.toString(), HttpMethod.GET, request,
                Project[].class);

        log.debug("Response: {}", response.getHeaders());
        int totalPages = Integer.parseInt(response.getHeaders().get("X-Total-Pages").get(0));
        log.debug("We've got {} pages of projects", totalPages);

        List<Project> list = new ArrayList<>();
        for (int i = 0; i < response.getBody().length; i++) {
            list.add(response.getBody()[i]);
        }

        for (int i = 2; i <= totalPages; i++) {
            log.debug("Fetching page {}", i);
            URI pageQuery = buildNextPageQuery(query, i);
            response = template.exchange(pageQuery.toString(), HttpMethod.GET, request, Project[].class);

            log.debug("Got another {} projects", response.getBody().length);
            for (int j = 0; j < response.getBody().length; j++) {
                list.add(response.getBody()[j]);
            }
        }
        return list;
    }
    
    @Override
    public List<Contributor> getContributors(RestTemplate template, HttpEntity<String> request, Integer projectId) {
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId.toString() + "/repository/contributors")
                .queryParam("per_page",100).build(false).toUri();
        
        ResponseEntity<Contributor[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Contributor[].class);
        
        int totalPages = Integer.parseInt(response.getHeaders().get("X-Total-Pages").get(0));
        
        List<Contributor> list = new ArrayList<>();
        list.addAll(Arrays.asList(response.getBody()).stream().collect(Collectors.toList()));
        
        for (int i = 2; i <= totalPages; i++) {
            URI pageQuery = buildNextPageQuery(query, i);
            response = template.exchange(pageQuery.toString(), HttpMethod.GET, request, Contributor[].class);

            list.addAll(Arrays.asList(response.getBody()).stream().collect(Collectors.toList()));
        }
        return list;
    }
    
    
    @Override
    public List<Event> getEventList(RestTemplate template, HttpEntity<String> request, Integer projectId) {
        
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.add(Calendar.YEAR, -1);
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId.toString() + "/events")
                .queryParam("action", "pushed")
                .queryParam("per_page", 100)
                .queryParam("after", df.format(cal.getTime())).build(false).toUri();
        
        ResponseEntity<Event[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Event[].class);
        
        int totalPages = Integer.parseInt(response.getHeaders().get("X-Total-Pages").get(0));
        
        List<Event> list = new ArrayList<>();
        list.addAll(Arrays.asList(response.getBody()).stream().collect(Collectors.toList()));
        
        for (int i = 2; i <= totalPages; i++) {
            URI pageQuery = buildNextPageQuery(query, i);
            response = template.exchange(pageQuery.toString(), HttpMethod.GET, request, Event[].class);

            list.addAll(Arrays.asList(response.getBody()).stream().collect(Collectors.toList()));
        }
        return list;
    }
    

    @Override
    public List<Issue> getProjectIssues(RestTemplate template, HttpEntity<String> request, Integer projectId, String state) {
        
        UriComponentsBuilder queryB = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId + "/issues");
        queryB.queryParam("per_page", 100);
        if (!StringUtils.isBlank(state)) {
            queryB.queryParam("state", state);
        }
        
        URI query = queryB.build(false).toUri();
        
        ResponseEntity<Issue[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Issue[].class);
        
        return Arrays.asList(response.getBody());
    }
    
    @Override
    public List<Member> getMemberList(RestTemplate template, HttpEntity<String> request, Integer projectId, Boolean inherited) {
        
        String path = "/api/v4/projects/" + projectId.toString() + "/members";
        path = inherited ? path + "/all" : path;
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path(path).build(false).toUri();
        
        ResponseEntity<Member[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Member[].class);
        
        return Arrays.asList(response.getBody()).stream().filter(i -> !i.getState().equalsIgnoreCase("blocked")).collect(Collectors.toList());
    }
    
    @Override
    public String getBranch(RestTemplate template, HttpEntity<String> request, Integer projectId) {
        
        String path = "/api/v4/projects/" + projectId.toString() + "/repository/branches";
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path(path).build(false).toUri();
        
        ResponseEntity<Branch[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Branch[].class);

        List<String> branches = Arrays.asList(response.getBody()).stream()
                .filter(i -> "main".equals(i.getName()) || "master".equals(i.getName()))
                .filter(i -> !i.getMerged())
                .map(Branch::getName)
                .collect(Collectors.toList());
        
        if (branches.size() == 1) {
            return branches.get(0);
        } else if (branches.size() > 1){
            return branches.stream().anyMatch(i -> "main".equals(i))? "main" : "master";
        }
            
        return "master";
    }
    
    @Override
    public String getFile(RestTemplate template, HttpEntity<String> request, Integer projectId, String filePath, String ref) {
        
        String query = "https://" + host + "/api/v4/projects/" + projectId + "/repository/files/{file_path}/raw?ref=" + ref;
        
        try {
            ResponseEntity<String> response = template.exchange(query, HttpMethod.GET, request, String.class, filePath);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            log.debug("Unable to fetch file for master {}: {} - {}", filePath, e.getStatusCode(), e.getResponseBodyAsString());
        }
        
        return null;
    }
    
    @Override
    public List<String> getRepoTree(RestTemplate template, HttpEntity<String> request, Integer projectId) {
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId + "/repository/tree").build(false).toUri();
        
        ResponseEntity<RepositoryTree[]> response = template.exchange(query.toString(), HttpMethod.GET, request, RepositoryTree[].class);
        
        return Arrays.asList(response.getBody()).stream().map(RepositoryTree::getName).collect(Collectors.toList());
    }
    
    @Override
    public List<Search> search(RestTemplate template, HttpEntity<String> request, Integer projectId, String term, String scope) {
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId + "/search")
                .queryParam("scope", scope)
                .queryParam("search", term)
                .build(false).toUri();
        
        ResponseEntity<Search[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Search[].class);
        
        return Arrays.asList(response.getBody());
    }
    
    @Override
    public List<Registry> getRegistryImages(RestTemplate template, HttpEntity<String> request, Integer projectId) {
        
        URI query = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects/" + projectId + "/registry/repositories").build(false).toUri();
        
        try {
            ResponseEntity<Registry[]> response = template.exchange(query.toString(), HttpMethod.GET, request, Registry[].class);
            return Arrays.asList(response.getBody());
        } catch (HttpClientErrorException e) {
            log.debug("Unable to list registry packages: {} - {}", e.getStatusCode(), e.getResponseBodyAsString());
        }
        return null;
    }
    
    @Override
    public URI buildProjectQuery() {
        
        UriComponentsBuilder builder = UriComponentsBuilder.newInstance().scheme("https").host(host).path("/api/v4/projects")
                .queryParam("order_by", "id") 
                .queryParam("page", 1)
                .queryParam("per_page", 100)
                .queryParam("sort", "asc")
                .queryParam("statistics", true)
                .queryParam("with_custom_attributes", false);
        
        if (archived != null) {
            builder.queryParam("archived", archived);
        }
        if (!StringUtils.isBlank(visibility)) {
            builder.queryParam("visibility", visibility);
        }
        if (!StringUtils.isBlank(search)) {
            builder.queryParam("search", search);
        }
        if (searchNamespaces != null) {
            builder.queryParam("search_namespaces", searchNamespaces);
        }
        if (owned != null) {
            builder.queryParam("owned", owned);
        }
        if (membership != null) {
            builder.queryParam("membership", membership);
        }
        if (starred != null) {
            builder.queryParam("starred", starred);
        }
        if (withIssuesEnabled != null) {
            builder.queryParam("with_issues_enabled", withIssuesEnabled);
        }
        if (withMergeRequestsEnabled != null) {
            builder.queryParam("with_merge_requests_enabled", withMergeRequestsEnabled);
        }
        if (!StringUtils.isBlank(withProgrammingLanguage)) {
            builder.queryParam("with_programming_language", withProgrammingLanguage);
        }
        if (wikiChecksumFailed != null) {
            builder.queryParam("wiki_checksum_failed", wikiChecksumFailed);
        }
        if (repositoryChecksumFailed != null) {
            builder.queryParam("repository_checksum_failed", repositoryChecksumFailed);
        }
        if (minAccessLevel != null) {
            builder.queryParam("min_access_level", minAccessLevel);
        }
        if (idAfter != null) {
            builder.queryParam("id_after", idAfter);
        }
        if (idBefore != null) {
            builder.queryParam("id_before", idBefore);
        }
        if (!StringUtils.isBlank(lastActivityAfter)) {
            builder.queryParam("last_activity_after", lastActivityAfter);
        }
        if (!StringUtils.isBlank(lastActivityBefore)) {
            builder.queryParam("last_activity_before", lastActivityBefore);
        }

        log.debug("Resulting query {}", builder.build(false).toUriString());
        
        return builder.build(false).toUri();
    }

    @Override
    public URI buildNextPageQuery(URI uri, int page) {
        
        log.trace("Resulting page query {}", UriComponentsBuilder.fromUri(uri).replaceQueryParam("page", page).build().toUriString());
        return UriComponentsBuilder.fromUri(uri).replaceQueryParam("page", page).build().toUri();
    }
}
