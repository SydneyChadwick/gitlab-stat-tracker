package za.co.pseudos.counter.service;

import java.text.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface Runner {

    void run() throws JsonProcessingException, ParseException;

}
