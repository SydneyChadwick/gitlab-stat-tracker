package za.co.pseudos.counter.service;

import java.net.URI;
import java.util.List;

import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

import za.co.pseudos.counter.dto.Contributor;
import za.co.pseudos.counter.dto.Event;
import za.co.pseudos.counter.dto.Issue;
import za.co.pseudos.counter.dto.Member;
import za.co.pseudos.counter.dto.Project;
import za.co.pseudos.counter.dto.Registry;
import za.co.pseudos.counter.dto.Search;

public interface GitApi {

    URI buildProjectQuery();

    URI buildNextPageQuery(URI uri, int page);

    List<Project> getProjectList(RestTemplate template, HttpEntity<String> request);

    List<Event> getEventList(RestTemplate template, HttpEntity<String> request, Integer projectId);

    List<Contributor> getContributors(RestTemplate template, HttpEntity<String> request, Integer projectId);

    List<Member> getMemberList(RestTemplate template, HttpEntity<String> request, Integer projectId, Boolean inherited);

    String getFile(RestTemplate template, HttpEntity<String> request, Integer projectId, String filePath, String ref); 

    List<String> getRepoTree(RestTemplate template, HttpEntity<String> request, Integer projectId);

    List<Search> search(RestTemplate template, HttpEntity<String> request, Integer projectId, String term,
            String scope);

    List<Registry> getRegistryImages(RestTemplate template, HttpEntity<String> request, Integer projectId);

    List<Issue> getProjectIssues(RestTemplate template, HttpEntity<String> request, Integer projectId, String state);

    String getBranch(RestTemplate template, HttpEntity<String> request, Integer projectId);

}
