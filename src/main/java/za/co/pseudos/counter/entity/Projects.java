package za.co.pseudos.counter.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Projects {

    @Id
    private Integer projectId;
    private String name;
    private String webUrl;
    private String created;
    private String lastUpdated;
    private Boolean archived;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar lastChecked;
    
    @Column(columnDefinition = "text")
    private String languageResponse;
    
    private String language;
    
    private Boolean cloneable;
    
    private Long repoSize;
    
    private Long storageSize;
    
    private Long wikiSize;
    
    private Long artifactSize;
    
    private Long openIssues;
    
    public Projects(Integer projectId) {
        this.projectId = projectId;
    }
}
