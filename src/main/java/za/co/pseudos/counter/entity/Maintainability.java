package za.co.pseudos.counter.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Maintainability {
    
    @Id
    private Integer projectId;
    
    @Column(columnDefinition = "text")
    private String results;
    
    public Maintainability(Integer projectId) {
        this.projectId = projectId;
    }
}
