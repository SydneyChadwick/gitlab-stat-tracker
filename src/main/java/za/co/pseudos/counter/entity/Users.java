package za.co.pseudos.counter.entity;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Users {

    @Id
    private Integer projectId;
    
    @Column(columnDefinition = "text")
    private String contributors;
    
    @Column(columnDefinition = "text")
    private String contributionsLastYear;
    
    @Column(columnDefinition = "text")
    private String members;
    
    @Column(columnDefinition = "text")
    private String inheritedMembers;
    
    public Users(Integer projectId) {
        this.projectId = projectId;
    }
}
