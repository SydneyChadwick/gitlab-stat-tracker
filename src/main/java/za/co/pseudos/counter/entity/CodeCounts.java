package za.co.pseudos.counter.entity;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CodeCounts {
    
    public CodeCounts(Integer projectId, String extension) {
        this.projectId = projectId;
        this.extension = extension;
    }

    @Id
    private String id;
    
    private Integer projectId;
    private String extension;
    
    private Integer value;
 
    @PrePersist
    public void prePersist() {
        this.id = UUID.randomUUID().toString();
    }
}
